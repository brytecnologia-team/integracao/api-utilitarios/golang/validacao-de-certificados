package model

import (
	"strconv"
	"strings"
)

type CertVerifyResponse struct {
	Nonce int
	Reports []struct{
		Key string
		Message string
		Nonce int
		Status struct {
			Status string
			CertificateStatusList []CertificateStatus
		}
	}
}

type CertificateStatus struct {
	Status string
	PkiBrazil bool
	ErrorStatus string
	RevocationStatus string
	CertificateInfo CertificateInfo
}

type CertificateInfo struct {
	SubjectDN struct{
		Cn string
	}
	Validity struct {
		NotBefore string
		NotAfter string
	}
}

func (r CertVerifyResponse) Report() string {
	sb := strings.Builder{}
	sb.WriteString("Amount of certificate verification reports: " + strconv.Itoa(len(r.Reports)) + "\n")
	for i := range r.Reports {
		sb.WriteString("Certificate Report {\n")
		sb.WriteString("\tNonce of timestamp: " +  strconv.Itoa(r.Reports[i].Nonce) + "\n")
		sb.WriteString("\tGeneral Chain Status: " + r.Reports[i].Status.Status + "\n")
		sb.WriteString("\tCertificate Status List: [\n")
		certStatusList := r.Reports[i].Status.CertificateStatusList
		for _, certStatus := range certStatusList {
			sb.WriteString(certStatus.Report())
		}
		sb.WriteString("\t]\n")
		sb.WriteString("}\n")
	}
	return sb.String()
}

func (cs CertificateStatus) Report() string {
	sb := strings.Builder{}
	sb.WriteString("\t\tCertificate Status {\n")
	sb.WriteString(cs.CertificateInfo.Report())
	sb.WriteString("\t\t\tCertificate Status: " + cs.Status + "\n")
	sb.WriteString("\t\t\tPki Brazil: " + strconv.FormatBool(cs.PkiBrazil) + "\n")
	if cs.Status == "INVALID" {
		sb.WriteString("\t\t\tError status: " + cs.ErrorStatus + "\n")
		sb.WriteString("\t\t\tRevocation status: " + cs.RevocationStatus + "\n")
	}
	sb.WriteString("\t\t}\n")
	return sb.String()
}

func (ci CertificateInfo) Report() string {
	sb := strings.Builder{}
	sb.WriteString("\t\t\tName: " + ci.SubjectDN.Cn + "\n")
	sb.WriteString("\t\t\tNot Before: " + ci.Validity.NotBefore + "\n")
	sb.WriteString("\t\t\tNot After: " + ci.Validity.NotAfter + "\n")
	return sb.String()
}
