package mapper

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"validacao-de-certificados/model"
)

type JsonMapper struct {}

func (jsonMapper JsonMapper) DecodeToString(response *http.Response) (string, error){
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func (jsonMapper JsonMapper) DecodeToCertVerifyResponse(response *http.Response) (*model.CertVerifyResponse, error) {
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	var certVerifyResponse model.CertVerifyResponse
	err = json.Unmarshal(bytes, &certVerifyResponse)
	return &certVerifyResponse, err
}
