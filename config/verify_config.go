package config

const (
	AccessToken = "<ACCESS_TOKEN>"
)

const(
	UrlCertVerification = "https://fw2.bry.com.br/api/validador-service/v1/certificateReports"
	//URL_HUB_SIGNER/api/validador-service/v1/certificateReports
)

const(
	Chain = "CHAIN"
	Basic = "BASIC"
)

const(
	ContentReturn = "true"
)

const(
	CertificatePath =  "./resource/certificate.cer"
)
