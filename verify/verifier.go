package verify

import (
	"fmt"

	"log"
	"net/http"
	"validacao-de-certificados/config"
	"validacao-de-certificados/mapper"
)

func VerifyCert(){
	params := map[string]string {
		"nonce": "1",
		"certificates[0][nonce]": "1",
		"mode": config.Chain,
		"contentReturn": config.ContentReturn,
	}
	files := map[string]string {
		"certificates[0][content]": config.CertificatePath,
	}

	req, err := NewFormDataRequest(config.UrlCertVerification, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	verifyReport(resp)
}
//case http status 200 - parses the json response and shows the verification report
func verifyReport(response *http.Response){

	if response.StatusCode == 200 {
		certResp, err := mapper.JsonMapper{}.DecodeToCertVerifyResponse(response)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(certResp.Report())
	} else {
		str, err := mapper.JsonMapper{}.DecodeToString(response)
		if err != nil {
			log.Fatal(err)
		}
		log.Fatal(str)
	}

}